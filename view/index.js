document.addEventListener('DOMContentLoaded', () => {
  // storing the output element to modify the output on the form
  const output = document.getElementById('output')
  // values that will be sent to the server
  let firstValue = ''
  let secondValue = ''
  let action = null
  let secretValue = ''
  // assigning click function to every number button
  // just addes its number to the output field
  // the length is limitted with 9 numbers (at least for now)
  for (let i = 0; i < 10; i++) {
    // getting button element by its id (button-0, button-1, button-2 etc.)
    let button = document.getElementById(`button-${i}`)
    // actual click event assignment
    button.addEventListener('click', () => {
      secretValue += i
      if (!action) {
        if (firstValue.length < 9) {
          // as firstValue is a string we can just add the number to the string and it will be a string
          firstValue += i
          output.innerHTML = `${output.innerHTML}${i}`
        }
      }
      else if (secondValue.length < 9) {
        secondValue += i
        output.innerHTML = `${output.innerHTML}${i}`
      }
      console.log('first: ', firstValue)
      console.log('second: ', secondValue)
    })
  }

  // cleaning up all values, action and the output field
  document.getElementById('clear-all').addEventListener('click', () => {
    firstValue = ''
    secondValue = ''
    action = null
    output.innerHTML = ''
    console.log('all clear')
  })

  // clearing the last value
  // if action was set, then clearing the second value
  // if not, then the first one
  document.getElementById('clear-last-var').addEventListener('click', () => {
    // checking if we have action set or the second value is already cleared
    if (!action || !secondValue.length) {
      // we take the substring excluding firstvalue
      // also we check if action is set (!! converts the type to boolean and + converts
      // boolean to number - 0 or 1) and removed it as well
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
      output.innerHTML = output.innerHTML.slice(0, (-1 * (firstValue.length + !!action)))
      firstValue = ''
      action = null
      console.log('cleared first value')
    } else {
      output.innerHTML = output.innerHTML.slice(0, (-1 * secondValue.length))
      secondValue = ''
      console.log('cleared second value')
    }
  })

  // removing the last character if it is not an action
  // if action is set then we work with second value
  // otherwise - with the first one
  document.getElementById('clear-last-char').addEventListener('click', () => {
    if (!action) {
      if (firstValue.length) {
        firstValue = firstValue.slice(0, -1)
        output.innerHTML = output.innerHTML.slice(0, -1)
        console.log('first value', firstValue)
      }
    } else {
      if (secondValue.length) {
        secondValue = secondValue.slice(0, -1)
        output.innerHTML = output.innerHTML.slice(0, -1)
        console.log('second value', secondValue)
      }
    }
  })

  // sending request to the server if the '=' was clicked
  // the data is split up to 3 values
  // for more info about fetch parameters, check the link
  document.getElementById('calculate').addEventListener('click', async () => {
    // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
    console.log('The request was sent to the server')
    const response = await fetch('http://localhost:3001/calculate', {
      method: 'PUT',
      mode: 'cors',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstValue: firstValue,
        secondValue: secondValue,
        action: action
      })
    })
    response.json().then(result => {
      console.log('reponse from the server: ', result)
      output.innerHTML = result.result
      firstValue = result.result
      secondValue = ''
      action = null
    })
  })

  // setting current action and clearing previous one if it was set
  const setAction = (newAction) => {
    // if action is the same then do nothing
    if (newAction === action) {
      return
    }

    // the list of all actions
    const actions = ['+', '-', '*', '/']
    //TODO: optimize using finding position of current action
    switch (newAction) {
      case 'divide':
        if (!action) {
          output.innerHTML = `${output.innerHTML}/`
        }
        // looking for other actions and replace them in the output if they were found
        // can be simplified but I am too sleepy to do so
        actions.forEach(_action => {
          output.innerHTML = output.innerHTML.replace(_action, '/')
        })
        console.log('action set to divide')
        break
      case 'multiply':
        if (!action) {
          output.innerHTML = `${output.innerHTML}*`
        }
        actions.forEach(_action => {
          output.innerHTML = output.innerHTML.replace(_action, '*')
        })
        console.log('action set to multiply')
        break
      case 'add':
        if (!action) {
          output.innerHTML = `${output.innerHTML}+`
        }
        actions.forEach(_action => {
          output.innerHTML = output.innerHTML.replace(_action, '+')
        })
        console.log('action set to add')
        break
      case 'sub':
        if (!action) {
          output.innerHTML = `${output.innerHTML}-`
        }
        actions.forEach(_action => {
          output.innerHTML = output.innerHTML.replace(_action, '-')
        })
        console.log('action set to sub')
        break
    }
    action = newAction
  }

  // setting actions for all type of calculations
  document.getElementById('divide').addEventListener('click', () => {
    setAction('divide')
  })

  document.getElementById('multiply').addEventListener('click', () => {
    setAction('multiply')
  })

  document.getElementById('add').addEventListener('click', () => {
    setAction('add')
  })

  document.getElementById('sub').addEventListener('click', () => {
    setAction('sub')
  })

  // adding a point to the number
  // checking if value is not empty, not full and no '.' is already in the number
  document.getElementById('float').addEventListener('click', () => {
    if (!action) {
      if (firstValue.length && firstValue.indexOf('.') < 0 && firstValue.length < 8) {
        firstValue = `${firstValue}.`
        output.innerHTML = `${output.innerHTML}.`
        console.log('float added to first value')
      }
    } else {
      if (secondValue.length && firstValue.indexOf('.' < 0) && secondValue.length < 8) {
        secondValue = `${secondValue}.`
        output.innerHTML = `${output.innerHTML}.`
        console.log('float added to second value')
      }
    }
    if (secretValue === '42069') {
      const colors = ['blueviolet', 'aquamarine', 'blue', 'coral', 'crimson', 'cyan', 'deeppink', 'indigo', 'mediumgreen', 'violet']
      let intervalTime = 300
      let interval
      let rotate = 10
      let positionX = 0
      let positionY = 0
      let scaleX = 1
      let scaleY = 1
      let skewX = 1
      let skewY = 1
      let rotateDirection = true
      let moveDirectionX = true
      let moveDirectionY = true
      let scaleDirectionX = true
      let scaleDirectionY = true
      let skewDirectionX = true
      let skewDirectionY = true
      const updateInterval = () => {
        if (interval) clearInterval(interval)
        if (intervalTime > 30) {
          intervalTime -= 8
        }

        if (rotateDirection) {
          rotate += 10
        } else {
          rotate -= 10
        }
        if (rotate > 420) {
          rotateDirection = false
        }
        if (rotate < -420) {
          rotateDirection = true
        }        
        
        if (moveDirectionX) {
          positionX += 12
        } else {
          positionX -= 8
        }
        if (positionX > 800) {
          moveDirectionX = false
        }
        if (positionX < 40) {
          moveDirectionX = true
        }

        if (moveDirectionY) {
          positionY += 9
        } else {
          positionY -= 13
        }
        if (positionY > 500) {
          moveDirectionY = false
        }
        if (positionY < 40) {
          moveDirectionY = true
        }

        if (scaleDirectionX) {
          scaleX += 0.08
        } else {
          scaleX -= 0.09
        }
        if (scaleX > 1.7) {
          scaleDirectionX = false
        }
        if (scaleX < 0.5) {
          scaleDirectionX = true
        }

        if (scaleDirectionY) {
          scaleY += 0.12
        } else {
          scaleY -= 0.11
        }
        if (scaleY > 1.7) {
          scaleDirectionY = false
        }
        if (scaleY < 0.5) {
          scaleDirectionY = true
        }

        if (skewDirectionX) {
          skewX += 3
        } else {
          skewX -= 4
        }
        if (skewX > 45) {
          skewDirectionX = false
        }
        if (skewX < -45) {
          skewDirectionX = true
        }  

        if (skewDirectionY) {
          skewY += 1
        } else {
          skewY -= 3
        }
        if (skewY > 45) {
          skewDirectionY = false
        }
        if (skewY < -45) {
          skewDirectionY = true
        }

        document.getElementById('calc-wrapper').style.transform = `translateX(${positionX}px) translateY(${positionY}px) rotate(${rotate}deg) scale(${scaleX}, ${scaleY}) skewX(${skewX}deg) skewY(${skewY}deg)`
        
        interval = setInterval(updateInterval, intervalTime)
      }
      interval = setInterval(updateInterval, intervalTime)
      setInterval(() => {
        document.getElementById('output').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('output').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.body.style.background = colors[Math.floor(Math.random() * colors.length)]
        for (let i = 0; i < 10; i++) {
          document.getElementById(`button-${i}`).style.background = colors[Math.floor(Math.random() * colors.length)]
          document.getElementById(`button-${i}`).style.color = colors[Math.floor(Math.random() * colors.length)]
        }
        document.getElementById('clear-all').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('clear-all').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('clear-last-var').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('clear-last-var').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('clear-last-char').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('clear-last-char').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('calculate').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('calculate').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('divide').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('divide').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('multiply').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('multiply').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('add').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('add').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('sub').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('sub').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('float').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('float').style.color = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('negate').style.background = colors[Math.floor(Math.random() * colors.length)]
        document.getElementById('negate').style.color = colors[Math.floor(Math.random() * colors.length)]
      
      }, 300)
    }
  })
})
