import * as fastify from 'fastify';
import * as fastifyBlipp from 'fastify-blipp';
import * as fastifyCors from 'fastify-cors';

import { routes } from './modules/core/routes';
import { UtilsService, ErrorHandlerService } from './modules/core/services'
import { FastifyServer } from 'modules/core/models';

const server: FastifyServer = fastify({
  // TODO: try to reduce body size by removing extensions before sending it here
  bodyLimit: 1038576 * 100,
  trustProxy: true,
  ajv: {
    customOptions: {
      coerceTypes: true,
      nullable: true
    }
  }
});

// applying some extra features to the server
server.register(fastifyBlipp);
server.register(fastifyCors, {
  origin: true,
  exposedHeaders: 'Content-Disposition'
});
// registering routes (calculate and status)
UtilsService.registerRoutes(server, routes);
// enable logging of erros
ErrorHandlerService.init(server);

// actual starting of the server on the port 3001 (or whatever set)
// server is start on http://localhost:3001
// u can check if it is running by typing http://localhost:3001/status in your browser
// it should return current date and working status
const start = async () => {
  try {
    await server.listen(+process.env.PORT || 3001, '0.0.0.0');
    server.blipp();
  } catch (err) {
    console.log(err);
    server.log.error(err);
    process.exit(1);
  }
};

process.on("uncaughtException", error => {
  console.error(error);
});
process.on("unhandledRejection", error => {
  console.error(error);
});

start();