export namespace UtilsService {
    export const registerRoutes = (server, routes) => {
        routes.forEach(route => server.register(route));
    }

    export const flatten = (object: any, prefix = ''): any => {
        if (isNil(object) || (typeof (object) === 'string') || (typeof (object)) === 'number') {
            return object;
        }

        return Object.keys(object).reduce((prev, element) => {
            return object[element] && typeof object[element] === 'object' && !Array.isArray(element)
                ? { ...prev, ...flatten(object[element], `${prefix}${element}.`) }
                : { ...prev, ...{ [`${prefix}${element}`]: String(object[element]) } };
        }, {});
    }
}

export const isNil = (value: any): boolean => {
    return value === undefined || value === null;
}

export const isNumeric = (value): boolean => {
    return !Array.isArray(value) && (value - parseFloat(value) + 1) >= 0;
}
