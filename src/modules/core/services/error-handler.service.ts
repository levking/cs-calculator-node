import { FastifyError } from 'fastify';

import { FastifyServer, Request, Reply } from '../models'

export namespace ErrorHandlerService {

    // casual error messages
    const ErrorMessages = {
        default: (code = 500, url?: string) => `Server answered with ${code} status code for ${url} request`,
        notFound: (url?: string) => `Server answered with 404. no such url ${url} was found.`,
        validationError: `Validation error occured`,
        missingBody: `No route data was provided for request`,
    }

    // enable error recognition for the server
    // it returns status to the client and logs error to the console
    export const init = (server: FastifyServer) => {
        server.setErrorHandler((error: FastifyError, request: Request, reply: Reply) => {
            const statusCode = error.statusCode;
            const { validation } = error;
            const response = {
                message: ErrorMessages.default(statusCode, request.raw.url),
                stack: error.stack,
                errorMessage: error.message,
                errors: validation ?? null
            };

            if (validation) {
                response.message = ErrorMessages.validationError;
            }

            if (!request.body) {
                response.message = ErrorMessages.missingBody;
            }
            reply.status(statusCode ?? 500).send(response.message);

            ErrorHandlerService.log('failed-http-request', error);
        });

        server.setNotFoundHandler((request: Request, reply: Reply) => {
            reply.status(404).send(ErrorMessages.notFound(request.raw.url));
        });
    }

    export const log = (name: string, payload: any) => {        
        console.log(name, payload);
    }
}