// default scheme checking
// can be removed as we are not using schemes here to validate the requests from client
export const SNumber = { type: 'number' };
export const SNumberNullable = { type: ['number', 'null'] };
export const SString = { type: 'string' };
export const SStringNullable = { type: ['string', 'null'] };
export const SBoolean = { type: 'boolean' };
export const SBooleanNullable = { type: ['boolean', 'null'] };