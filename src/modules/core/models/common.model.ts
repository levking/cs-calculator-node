import { FastifyRequest, DefaultParams, DefaultHeaders, DefaultBody, DefaultQuery, FastifyReply, FastifyInstance } from 'fastify';
import { IncomingMessage, ServerResponse, Server } from 'http';

// some types used by fastify
// declared for easier usage in the code
export type Request = FastifyRequest<IncomingMessage, DefaultQuery, DefaultParams, DefaultHeaders, DefaultBody>
export type Reply = FastifyReply<ServerResponse>;
export type Dictionary<T> = { [id: string]: T };
export type FastifyServer = FastifyInstance<Server, IncomingMessage, ServerResponse>;
