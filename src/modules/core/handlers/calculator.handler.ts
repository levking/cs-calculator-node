import { Request, Reply } from 'modules/core/models';

// The services for proccessing calculation data should be handled here
export async function calculatorHandler(request: Request, reply: Reply) {
    const {firstValue, secondValue, action} = request.body
    try {
        let parsedFirstValue: number
        let parsedSecondValue: number
        if (firstValue.indexOf('.') >= 0 || secondValue.indexOf('.') >= 0) {
            parsedFirstValue = parseFloat(firstValue)
            parsedSecondValue = parseFloat(secondValue)
        } else {
            parsedFirstValue = parseInt(firstValue)
            parsedSecondValue = parseInt(secondValue)
        }
    
        let result: string
        switch (action) {
            case 'multiply':
                result = `${(parsedFirstValue * parsedSecondValue).toFixed(2)}`
                break
            case 'divide':
                result = `${(parsedFirstValue / parsedSecondValue).toFixed(2)}`            
                break
            case 'add':
                result = `${parsedFirstValue + parsedSecondValue}`
                break
            case 'sub':
                result = `${parsedFirstValue - parsedSecondValue}`
                break
            default:
                result = 'Calculation error'
                break
        }
        return { result };
    } catch {
        return { result: 'Error happened :('}
    }

}
