import { Request, Reply } from 'modules/core/models';

// Returns current date and time
// used to check if the server is running
export async function statusHandler(request: Request, reply: Reply) {
    return { date: new Date(), works: true };
}
