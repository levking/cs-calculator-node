import { default as status } from './status.route';
import { default as calculator } from './calculator.route';

export const routes = [status, calculator];