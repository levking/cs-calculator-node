import * as fp from 'fastify-plugin';
import { statusHandler } from '../handlers';

// default template for registering a route
export default fp(async (server, opts, next) => {
  server.route({
    url: '/status',
    logLevel: 'warn',
    method: ['GET', 'HEAD'],
    handler: statusHandler
  });
  next();
});
