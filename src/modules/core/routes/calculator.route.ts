import * as fp from 'fastify-plugin';
import { calculatorHandler } from '../handlers/calculator.handler';

// default template for registering a route
export default fp(async (server, opts, next) => {
    server.route({
        url: '/calculate',
        logLevel: 'warn',
        method: ['PUT'],
        handler: calculatorHandler
    });
    next();
});
